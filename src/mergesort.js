class MergeSorter {
	constructor() {
		this.numberOfOperations = 0;
		this.swaps = [];
	}
	static name() {
		return 'Merge Sort';
	}
	sort(list) {
		this._mergeSort(list, 0, list.length)
	}
	_mergeSort(list, left, right) {
		if ((right-left) <=1) {
			return;
		}
		var mid = parseInt(left + (right-left)/2);
		this._mergeSort(list, left, mid);
		this._mergeSort(list, mid, right);
		this._merge(list, left, mid, right);
	}
	_merge(list, left, mid, right) {
		var leftList = [];
		for(let i = left; i < mid; i++) {
			leftList.push(i);
		}
		var rightList = [];
		for(let i = mid; i < right; i++) {
			rightList.push(i);
		}
		for(let i = left; i < right; i++) {

			if (leftList.length <1) {
				this.swap(list, i, rightList[0]);
				rightList[rightList.indexOf(i)] = rightList[0];
				rightList.shift();
			}
			else if( rightList.length < 1) {
				this.swap(list, i, leftList[0]);
				leftList[leftList.indexOf(i)] = leftList[0];
				leftList.shift();
			}
			else if (list[leftList[0]] < list[rightList[0]]) {
				this.swap(list, i, leftList[0]);
				leftList.indexOf(i) > -1 ? leftList[leftList.indexOf(i)] = leftList[0] : rightList[rightList.indexOf(i)] = leftList[0];
				leftList.shift();
			}
			else if (list[rightList[0]] <= list[leftList[0]]) {
				this.swap(list, i, rightList[0]);
				leftList.indexOf(i) > -1 ? leftList[leftList.indexOf(i)] = rightList[0] : rightList[rightList.indexOf(i)] = rightList[0];
				rightList.shift();
			}
		}
	}
  swap(list, i, j) {
	if (i == j) {return;}
    this.swaps.push({from: i, to: j});
    const tmp = list[i];
    list[i] = list[j];
    list[j] = tmp;
    return list;
  }

}
