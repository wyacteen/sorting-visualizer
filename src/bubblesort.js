class BubbleSorter {

  constructor() {
    this.numberOfOperations = 0;
    this.swaps = [];
  }

  static name() {
    return 'Bubble Sort';
  }
  sort(list)  {
    for(let i = list.length - 1; i >= 0; i--) {
      for(let j = 0; j < list.length - 1; j++) {
        if (list[j + 1] < list[j]) {
          this.swap(list, j+ 1, j);
        }
      }
    }
  };

  swap(list, i, j) {
    this.swaps.push({from: i, to: j});
    const tmp = list[i];
    list[i] = list[j];
    list[j] = tmp;
    return list;
  }
}


//const a = [3,2,1];
//const b = new BubbleSorter();
//b.sort(a);
//console.log(a);






