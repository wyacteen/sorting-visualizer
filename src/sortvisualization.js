class SortVisualizer {
  /**
   *
   * @param {Array} arr      - the unsorted array
   * @param {*} delay        - the animation delay in ms
   * @param {*} sorter       - the sorter
   * @param {*} container    - the jQuery object container
   * @param {*} swapCallback - callback for stats
   */
  constructor(arr, delay, sorter, container, swapCallback) {
    this.arr = (arr.slice(0, arr.length));
    this.container = container;
    this.swapCallback = swapCallback;
    this.sorter = sorter;
    this.currentOperations = 0;
    this.animationDelay(delay);
    this.setSoundDuration();
    this.renderRectangles();
    this.audioContext = new (window.AudioContext || window.webkitAudioContext)();
    this.sorter.sort(this.arr.slice(0, this.arr.length));
  }

  createDiv(index, val) {
    return '<div class="array-value '
      + 'index_' + index
      + '" style="color:white; width:'
      + this.computeWidthPercentage(val)
      + '%' + '; height:'
      + this.computeHeight()
      + 'px;"></div>';
  }

  setDivValues(index, toVal) {
    let div = this.container.find(this.getClassForIndex(index));
    div.width(this.computeWidthPercentage(toVal) + '%');
  }

  renderRectangles() {
    this.clearContainer();
    for (let i = 0; i < this.arr.length; i++) {
      let d = this.createDiv(i, this.arr[i]);
      this.container.append(d);
    }
  }

  setSoundDuration() {
    if (this.animationDelay() <= 100) {
      this.soundDuration = 100;
    }
    else {
      this.soundDuration = Math.min(this.animationDelay()/2, 100);
    }
  }

  computeHeight() {
    return Math.max(1, parseInt(600/this.arr.length));
  }

  computeWidthPercentage(val) {
    const scaledValue = val/Math.max(...this.arr);
    return scaledValue * 100;
  }
  computeWidth(val) {
    if (this.arr.length > 100 && this.arr.length > 50) {
      return val * 3;
    }
    else if (this.arr.length > 25) {
      return val * 10;
    }
    else {
      return val * 25;
    }

  }

  getNoteFrequency(val) {
    const base = 110;
    const noteRatios = [
      1,
      9/8,
      81/64,
      3/2,
      27/16
    ];

    const octave = Math.ceil(val/5);
    const note = val % 5;
    const frequency = base * noteRatios[note] * octave * 1.25;
    return frequency;

  }

  animateSwap(from, to) {
    const fromVal = this.arr[from];
    const toVal = this.arr[to];

    this.swap(from, to);
    if (this.swapCallback) {
      this.swapCallback({
        operationCount: ++this.currentOperations,
        elapsedMilliseconds: performance.now() - this.startTime
      });
    }

    this.setDivValues(from, toVal);
    this.setDivValues(to, fromVal);
    this.playNote(this.getNoteFrequency(fromVal), this.soundDuration);

  }

  swap(from, to) {
    let tmp = this.arr[from];
    this.arr[from] = this.arr[to];
    this.arr[to] = tmp;
  }

  animationDelay(val) {
    if (typeof val !== "undefined") {
      this._animationDelay = val;
    }
    return this._animationDelay;
  }

  clearContainer() {
    this.container.empty();
  }

  start() {
    this.startTime = performance.now();
    this.animateSwaps();
  }

  /**
   * Plays a note of the specified frequency and durations.
   *
   * @param {int} frequency - frequency in Hz
   * @param {int} duration  - duration in ms
   */
  playNote(frequency, duration) {
    var oscillator = this.audioContext.createOscillator();

    oscillator.type = 'square';
    oscillator.frequency.value = frequency;
    oscillator.connect(this.audioContext.destination);
    oscillator.start();

    setTimeout(
      function(){
        oscillator.stop();
      }, duration);
  }

  getClassForIndex(index) {
    return ".index_" + index;
  }

  getClassFromIndex(index) {
    return "index_" + index;
  }

  getDivForIndex(index) {
    const div = this.container.find(this.getClassForIndex(index));
    if(div.length === 0) {
      alert('oops');
    }
    return div;
  }

  markSwapActive(swap) {
    this.getDivForIndex(swap.from).toggleClass('active', true);
    this.getDivForIndex(swap.to).toggleClass('active', true);
  }

  markSwapInactive(swap) {
    this.getDivForIndex(swap.from).toggleClass('active', false);
    this.getDivForIndex(swap.to).toggleClass('active', false);
  }

  sortCompletedAnimation(start) {
    var t = this;
    if (start >= this.arr.length) {
      return;
    }

    let from = start || 0;
    this.markSwapActive({from: from, to: from});
    this.playNote(this.getNoteFrequency(this.arr[from]), this.soundDuration);
    requestAnimationFrame(function() {
      t.markSwapInactive({from: from, to: from});
      t.sortCompletedAnimation(from + 1);
    });
  }

  animateSwaps() {
    var t = this;
    let id = requestAnimationFrame(function() {
      if (t.sorter.swaps.length) {
        const swap = t.sorter.swaps.shift();
        t.markSwapActive(swap);
        t.animateSwap(swap.from, swap.to);
        setTimeout(function() {
          t.markSwapInactive(swap);
          t.animateSwaps();
        }, t.animationDelay());
      }
      else {
        cancelAnimationFrame(id);
        t.sortCompletedAnimation();
      }
    });
  }

  end() {
    t.endTime = performance.now();
  }

  static createArray(size, start) {
    const arr = [];
    for(let i = start; i < start + size; i++) {
      arr.push(i);
    }
    return arr;
  }

  static createRandomizedArray(size) {
    const arr = SortVisualizer.createArray(size, 1);
    this.shuffleArray(arr);
    return arr;
  }

  static shuffleArray(arr) {
    for (let i = 0; i < arr.length; i++) {
      const tmp = arr[i];
      const swapIndex = Math.floor(Math.random() * (arr.length));
      arr[i] = arr[swapIndex];
      arr[swapIndex] = tmp;
    }
    return arr;
  }
}



