
class QuickSorter {

  constructor() {
    this.numberOfOperations = 0;
    this.swaps = [];
  }

  static name() {
    return 'Quicksort';
  }
  sort(list)  {
    this._quicksort(list, 0, list.length -1);
    return;
  };

  swap(list, i, j) {
    this.swaps.push({from: i, to: j});
    const tmp = list[i];
    list[i] = list[j];
    list[j] = tmp;
    return list;
  }

  _quicksort(list, left, right) {
    if (left >= right) {
      return;
    }

    const pivot = list[right];
    let swapPosition = left;
    let i = left;

    while(i <= right - 1) {
      if (list[i] >= pivot) {
        i++;
      }
      else if (list[i] < pivot) {
        this.swap(list,i, swapPosition);
        i++;
        swapPosition++;
      }
      this.numberOfOperations++;
    }
    this.swap(list, swapPosition, right);
    this.numberOfOperationsoperations++;
    this._quicksort(list, left, swapPosition - 1);
    this._quicksort(list, swapPosition + 1, right);
  }
}






